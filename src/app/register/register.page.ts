import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import * as firebase from 'firebase';
import { AlertController, NavController } from '@ionic/angular';
@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  
  form= new FormGroup({
    'email':new FormControl,
    'password':new FormControl,
    'confirmPassword':new FormControl
  })
  
  constructor(private alertController:AlertController,private navController:NavController) { 
    //É necessário que toda página possua o "firebase.auth().getRedirectResult()" pois é esse código que mantém o usuário logado
    firebase.auth().getRedirectResult()
    .then(()=>{
      if(firebase.auth().currentUser!=null)
      {
        this.navController.navigateRoot('/home');
      }
    })
    ////
    console.log(firebase.auth().currentUser);

  }

  ngOnInit() {
  }
  
  registerWithEmailAndPassword()
  {
    let email= this.form.get('email').value as string;
    let password=this.form.get('password').value as string;
    
    //Define a linguagem para o idioma do usuário.Importante pois será enviado um email
    firebase.auth().useDeviceLanguage();
    
    //Função que ativa o cadastro do usuário
    //Requer como paramêtros duas string,uma pra email e outra para senha
    //Retorna uma promessa
    firebase.auth().createUserWithEmailAndPassword(email,password)
    .then(async ()=>{
    //Código ativado se o cadastro for realizado com sucesso
    firebase.auth().currentUser.sendEmailVerification()
    .then(async ()=>{
      const alert= await this.alertController.create({
          header:"Cadastro realizado com sucesso!",
          message:"O usuário foi cadastrado com sucesso.\nUm email de verificação será enviado para sua caixa de entrada.\nPor favor,verifique seu email para proseguir com o login",
          buttons:[
           {
              text:'Ok!',
              handler:(()=>{
                this.navController.navigateRoot('/login');//Obs: Verificar se no programa em especifica essa rota condiz
              })
           },
         ]
      });
      await alert.present();

    })
    //Essa parte do erro no envio do email pode ser muito melhorada
    .catch((err)=>{
      alert(err);
    })
    })
    .catch(async (err)=>{
    //Código ativado se ocorrer umm erro
    if( err.code=='auth/email-already-in-use' || err.code=='auth/invalid-email' || err.code=='auth/operation-not-allowed' || err.code=='auth/weak-password')
    {
      //Aviso de erros para os erros padrões
      if(err.code=='auth/email-already-in-use')
      {        
        const alert= await this.alertController.create({
          header:"Erro no cadastro",
          message:"Este email já está associado com uma conta.Por favor,tente novamente",
          buttons:[
            {
              text:'Ok',
            },
          ]
        });
        await alert.present();
      }
      
      if(err.code=='auth/invalid-email')
      {
        const alert= await this.alertController.create({
          header:"Erro no cadastro",
          message:"Este email não é válido.Por favor,digite novamente",
          buttons:[
            {
              text:'Ok',
            },
          ]
        });
        await alert.present();
      }
      
      if(err.code=='auth/operation-not-allowed')
      {
        const alert= await this.alertController.create({
          header:"Erro no cadastro",
          message:"No momento não é possível realizar cadastro com email e senha.",
          buttons:[
            {
              text:'Ok',
            },
          ]
        });
        await alert.present();
      }
      
      if(err.code=='auth/weak-password')
      {
        const alert= await this.alertController.create({
          header:"Erro no cadastro",
          message:"A senha está muito fraca.Por favor,utilize uma senha mais forte",
          buttons:[
            {
              text:'Ok',
            },
          ]
        });
        await alert.present();
      }
    }
    else
    {
      //Código para lidar com erros inesperados
      const alert= await this.alertController.create({
        header:"Erro no cadastro",
        message:"Um erro inesperado ocorreu. Por favor,tente novamente em instantes",
        buttons:[
          {
            text:'Ok',
          },
        ]
      });
      await alert.present();
    }
    })
  }

  registerWithGoogle(){
    firebase.auth().useDeviceLanguage();

    let provider= new firebase.auth.GoogleAuthProvider();
    
    firebase.auth().signInWithRedirect(provider)
    .then(()=>{
    //Código ativado se o cadastro for realizado com sucesso
    firebase.auth().currentUser.sendEmailVerification()
    .then(async ()=>{
      const alert= await this.alertController.create({
          header:"Cadastro realizado com sucesso!",
          message:"O usuário foi cadastrado com sucesso.\nUm email de verificação será enviado para sua caixa de entrada.\nPor favor,verifique seu email para proseguir com o login",
          buttons:[
           {
              text:'Ok!',
              handler:(()=>{
                this.navController.navigateRoot('/login');//Obs: Verificar se no programa em especifica essa rota condiz
              })
           },
         ]
      });
      await alert.present();

    })
    //Essa parte do erro no envio do email pode ser muito melhorada
    .catch((err)=>{
      alert(err);
    })
    })

  }



}
