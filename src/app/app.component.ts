import { Component } from '@angular/core';
import * as firebase from 'firebase';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'List',
      url: '/list',
      icon: 'list'
    },
    {
      title: 'Login',
      url: '/login',
      icon: 'list'
    },
    {
      title: 'Register',
      url: '/register',
      icon: 'list'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
    firebase.initializeApp({
    apiKey: "AIzaSyB2GcrcbUwNg4Nf5BY4uibD7_D6KQBPLRA",
    authDomain: "projetomanual-daaab.firebaseapp.com",
    databaseURL: "https://projetomanual-daaab.firebaseio.com",
    projectId: "projetomanual-daaab",
    storageBucket: "projetomanual-daaab.appspot.com",
    messagingSenderId: "23320256050",
    appId: "1:23320256050:web:50a28dddcd6ac6b5"
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
