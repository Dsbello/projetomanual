import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  
  user=firebase.auth().currentUser;

  constructor(){
    //É necessário que toda página possua o "firebase.auth().getRedirectResult()" pois é esse código que mantém o usuário logado
    firebase.auth().getRedirectResult()
    .then(()=>{
      console.log(firebase.auth().currentUser);
    })

  }

  logOut(){
    firebase.auth().signOut()
    .then(()=>{
      console.log("Usuario deslogado");
    })
  }
}
