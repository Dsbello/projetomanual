import { AppServicesService } from './../app-services.service';
import { NavController, AlertController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import * as firebase from 'firebase';
import { PhoneNumber } from './phone-number/phone-number.component';



@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  
  form=new FormGroup({
    'email':new FormControl,
    'password':new FormControl
  });
  
  ///Necessário se houver confirmação por telefone
  windowRef:any;
  
  async loginWithPhoneNumber(){
    ///Alerta criado para o envio de código de verificação
    let alert = await this.alertController.create({
      header: 'Login com Telefone',
      message:'Digite o número de seu telefone,por favor',
      inputs: [
        {
          name: 'countryCode',
          placeholder: '55'
        },
        {
          name: 'areaCode',
          placeholder: '21'
        },
        {
          name: 'phoneNumber',
          placeholder: '999999999'
        },
        {
          name:'verificationCode',
          placeholder:'Código de verificação'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Receber código',
          role:'null',
          handler: data => {
            //Função que envia o código para o telefone do usuário
            const appVerifier = this.windowRef.recaptchaVerifier;
            let num:string='+' +(data.countryCode)+(data.areaCode)+(data.phoneNumber)            
            //Note que um dos argumentos é "appVerifier",ou seja, é necessário que o captcha tenha sido realizado
            firebase.auth().signInWithPhoneNumber(num,appVerifier)
            .then(result =>{
              this.windowRef.confirmationResult = result;
            })
            .catch((error) =>{
              console.log(error);
            } 
            );
            //Return false faz com que o alertController não feche automaticamente
            return false
          }
        },
        {
          text:'Confirmar código',
          handler:(data)=>{
            ///Função que confirma o código de verificação
              this.windowRef.confirmationResult
              //Note que o parâmetro é o valor do input "verificationCode"
              .confirm(data.verificationCode)
              .then( (result) => {
              ///Direciona o usuário para a página inicial,caso a verificação seja realizada com sucesso
                this.navController.navigateRoot('/home');
              ///
            })
            .catch( (error) => console.log(error, "Incorrect code entered?"));
            ///
          }
        }
      ]
    });
    alert.present();
    ///
  }
  //+códigos em ngOnInte e constructor
  ///
  
  constructor(private navController:NavController,private alertController:AlertController,private win:AppServicesService) { 
    ///É necessário que toda página possua o "firebase.auth().getRedirectResult()" pois é esse código que mantém o usuário logado
    firebase.auth().getRedirectResult()
    .then(()=>{
      if(firebase.auth().currentUser!=null)
      {
        this.navController.navigateRoot('/home');
      }
    })
    ///private win:AppServicesService necessário se houver confirmação por telefone
  }
  ngOnInit() {
    ///Códigos necessários se houver confirmação por telefone
    this.windowRef=this.win.WindowRef;
    this.windowRef.recaptchaVerifier= new firebase.auth.RecaptchaVerifier('recaptcha-container');
    this.windowRef.recaptchaVerifier.render();
    ///
  }
  
  ///Necessário se houver confirmação por email e senha
  loginWithEmailAndPassword(){
    //Define os argumentos que serão passados para a função
    let email=this.form.get('email').value as string;
    let password=this.form.get('password').value as string;
    //Define o idioma para o idioma do usuário
    firebase.auth().useDeviceLanguage();

    //Função que loga o usuário.
    //Pede como argumento 2 strings: email e senha
    //Retorna uma promise
    
    firebase.auth().signInWithEmailAndPassword(email,password)
      .then(async ()=>{
        if(firebase.auth().currentUser.emailVerified==true)
        {
          this.navController.navigateRoot("/home");
        }
        else
        {
          const alert= await this.alertController.create({
            header:"Email não verificado",
            message:"Esse email ainda não foi validado.Por favor,verifique sua caixa de mensagens e valide seu email.",
            buttons:[
              {
                text:'Reenviar email de validação',
                handler:()=>{
                  firebase.auth().currentUser.sendEmailVerification()
                  .then(()=>{
                    firebase.auth().signOut();
                  })
                  .catch((err)=>{
                    console.log("Ocorreu um erro");
                    console.log(err);
                  })
                }
              },
              {
                text:'Voltar para login',
              }
            ]
          });
          await alert.present();
        }
      })
      .catch(async (err)=>{
        if(err.code=='auth/invalid-email' || err.code=='auth/user-disabled' || err.code=='auth/user-not-found' || err.code=='auth/wrong-password')
    {
      //Aviso de erros para os erros padrões
      
      if(err.code=='auth/invalid-email')
      {
        const alert= await this.alertController.create({
          header:"Erro no cadastro",
          message:"Este email não é válido.Por favor,digite novamente",
          buttons:[
            {
              text:'Ok',
            },
          ]
        });
        await alert.present();
      }

      if(err.code=='auth/user-disabled')
      {
        const alert= await this.alertController.create({
          header:"Erro no login",
          message:"Este email está desabilitado.Por favor,reabilite a sua conta e tente novamente",
          buttons:[
            {
              text:'Ok',
            },
          ]
        });
        await alert.present();
      }

      if(err.code=='auth/user-not-found')
      {
        const alert= await this.alertController.create({
          header:"Erro no login",
          message:"Este email não foi encontrado.Por favor,digite um email válido",
          buttons:[
            {
              text:'Ok',
            },
          ]
        });
        await alert.present();
      }

      if(err.code=='auth/wrong-password')
      {
        const alert= await this.alertController.create({
          header:"Erro no cadastro",
          message:"O email e a senha fornecida não combinam.",
          buttons:[
            {
              text:'Ok',
            },
          ]
        });
        await alert.present();
      }
      
        }
        else
    {
      //Código para lidar com erros inesperados
      const alert= await this.alertController.create({
        header:"Erro no cadastro",
        message:"Um erro inesperado ocorreu. Por favor,tente novamente em instantes",
        buttons:[
          {
            text:'Ok',
          },
        ]
      });
      await alert.present();
        }
      })
  }
    ///Função que muda a senha
      async changePasswordForEmailAndPassword(){
    
    //Alerta para o envio do email de mudar senha
      let alert = await this.alertController.create({
        header: 'Esqueci a senha',
        message:'Um email será enviado para sua caixa de entrada.Por favor,digite seu email',
        inputs: [
          {
            name: 'email',
            placeholder: 'email'
          },
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
          },
          {
            text: 'Enviar email',
            handler: data => {
              //Função que de fato envia o email de resetPassword.Repare que data.email é o parâmetro da função,equivalente ao input "email"
              firebase.auth().sendPasswordResetEmail(data.email);
            }
          }
        ]
      });
      alert.present();
    ///
  
  
  }
  
  ///Necessário se houver confirmação pela conta do google
  loginWithGoogle(){
    //Define o idioma para o idioma do usuário
    firebase.auth().useDeviceLanguage();

    //Defina o provider de onde as credenciais serão retiradas
    let provider = new firebase.auth.GoogleAuthProvider();
    
    //Função que faz o cadrastro,loga o usuário e já autentica o email
    firebase.auth().signInWithRedirect(provider)
    .then(()=>{
    })
    .catch(()=>{})
  }
  ///
  
   ///Necessário se houver confirmação pela conta do google
  loginWithFacebook(){
  //Define o idioma para o idioma do usuário
  firebase.auth().useDeviceLanguage();

  //Defina o provider de onde as credenciais serão retiradas
  let provider = new firebase.auth.FacebookAuthProvider();
  
  //Função que faz o cadrastro,loga o usuário e já autentica o email
  firebase.auth().signInWithRedirect(provider)
  .then(()=>{
  })
  .catch(()=>{})
}
///

}
