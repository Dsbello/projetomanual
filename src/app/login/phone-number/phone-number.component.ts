import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { AppServicesService } from 'src/app/app-services.service';

export class PhoneNumber {
  country:string;
  ddd:string;
  line:string;

  //format phone numbers as E.164
  get e164(){
    const num='+'+this.country+this.ddd+this.line
    return num;
  }
}

@Component({
  selector: 'phone-number',
  templateUrl: './phone-number.component.html',
  styleUrls: ['./phone-number.component.scss'],
})
export class PhoneNumberComponent implements OnInit {

  windowRef:any;
  phoneNumber =new PhoneNumber;
  verificationCode:string;
  user:any;
  
  form=new FormGroup({
    'phoneNumber.country':new FormControl,
    'phoneNumber.area':new FormControl,
    'phoneNumber.prefix':new FormControl,
    'phoneNumber.line':new FormControl,
    'verificationCode':new FormControl

  })
  constructor(private win:AppServicesService) { }

  ngOnInit() {
    this.windowRef=this.win.WindowRef;
    this.windowRef.recaptchaVerifier= new firebase.auth.RecaptchaVerifier('recaptcha-container');

    this.windowRef.recaptchaVerifier.render();
  }

  sendLoginCode(){
    const appVerifier = this.windowRef.recaptchaVerifier;
    const num = this.phoneNumber.e164;

    firebase.auth().signInWithPhoneNumber(num,appVerifier)
    .then(result =>{
      this.windowRef.confirmationResult = result;
    })
    .catch(error => console.log(error) );
  }

  verifyLoginCode(){
    this.windowRef.confirmationResult
    .confirm(this.form.get('verificationCode'))
    .then( result => {
                      this.user = result.user;
  })
  .catch( error => console.log(error, "Incorrect code entered?"));
  
}

}
